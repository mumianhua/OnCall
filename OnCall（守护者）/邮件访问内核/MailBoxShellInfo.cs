﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LumiSoft.Net.Mail;

namespace 邮件访问内核
{
    /// <summary>
    /// 邮件访问内核信息
    /// </summary>
    public class MailBoxShellInfo
    {
        /// <summary>
        /// 用户帐号
        /// </summary>
        public string User;
        /// <summary>
        /// 密码
        /// </summary>
        public string Password;
        /// <summary>
        /// 服务器地址
        /// </summary>
        public string Host;
        /// <summary>
        /// 端口号
        /// </summary>
        public int Port;

        /// <summary>
        /// 刷新延时
        /// </summary>
        public int Delay;
        /// <summary>
        /// 刷新次数
        /// </summary>
        public long Frequency;
        /// <summary>
        /// 等待时间
        /// </summary>
        public long WaitTime { get { return Delay * Frequency; } }

        /// <summary>
        /// 特征字符串
        /// </summary>
        public string Feature;

        /// <summary>
        /// 完成后是否删除
        /// </summary>
        public bool DeleteWhenFinished;

        /// <summary>
        /// 是否工作中
        /// </summary>
        public bool isWorking;

        /// <summary>
        /// 马上刷新
        /// </summary>
        public bool RefreshNow;

        /// <summary>
        /// 上次检查时间
        /// </summary>
        public DateTime LastChekedTime;

        /// <summary>
        /// 最后的邮件ID
        /// </summary>
        public string LastUID;

        /// <summary>
        /// 管理员
        /// </summary>
        public string Administrator;

        public MailBoxShellInfo()
        {
            Init();
        }

        /// <summary>
        /// 初始化
        /// </summary>
        public void Init()
        {
            User = string.Empty;
            Password = string.Empty;
            Host = string.Empty;
            Port = 110;
            Delay = 500;
            Frequency = 20;
            Feature = string.Empty;
            DeleteWhenFinished = true;
            isWorking = false;
            RefreshNow = false;
            Administrator = "";
            LastChekedTime = DateTime.Now;
        }
    }
}
