﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace 命令处理机
{
    public class Test
    {
        public static void test(CommandInfo info)
        {
            Console.WriteLine("--------Command--------");
            Console.WriteLine("Name:[{0}]", info.Command);
            Console.WriteLine("Number of args is {0}", info.Args.Length);
            if (info.isSingleLineType)
            {
                Console.WriteLine(info.Command + " " + string.Join(" ", info.Args));
            }
            else
            {
                Console.WriteLine(info.Args[0]);
            }
        }
    }
}
