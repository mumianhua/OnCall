﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using 邮件访问内核;

namespace 前台界面
{
    public partial class MainWindow : Form
    {
        private MailBoxAccessShell mailBoxShell;
        private MailBoxShellInfo shellInfo;
        public MainWindow(MailBoxAccessShell m)
        {
            mailBoxShell = m;
            shellInfo = mailBoxShell.shellInfo;
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;
        }

        /// <summary>
        /// 邮箱设置区域使能
        /// </summary>
        private void MailBoxSettingEnable()
        {
            btnApply.Enabled = true;
            btnCancel.Enabled = false;
            btnStop.Enabled = false;
            txtbxUser.ReadOnly = false;
            txtbxPwd.ReadOnly = false;
            txtbxHost.ReadOnly = false;
            nudPort.Enabled = true;

            statusLabel.Text = "未工作";
            txtbxUser.Text = shellInfo.User;
            txtbxPwd.Text = shellInfo.Password;
            txtbxHost.Text = shellInfo.Host;
            nudPort.Value = shellInfo.Port;
            txtbxUser.Select();
        }

        /// <summary>
        /// 邮箱设置区域失效
        /// </summary>
        private void MailBoxSettingUnEnable()
        {
            btnApply.Enabled = false;
            btnCancel.Enabled = true;
            txtbxUser.ReadOnly = true;
            txtbxPwd.ReadOnly = true;
            txtbxHost.ReadOnly = true;
            nudPort.Enabled = false;
            btnBegin.Select();
        }

        /// <summary>
        /// 命令设置区域使能
        /// </summary>
        private void SettingEnable()
        {
            btnBegin.Enabled = true;
            btnCancel.Enabled = true;
            btnStop.Enabled = false;
            txtbxFeature.ReadOnly = false;
            txtbxAdmin.ReadOnly = false;
            nudWaitTime.ReadOnly = false;
            nudWaitTime.Enabled = true;
            deleteMail.Enabled = true;

            statusLabel.Text = "就绪";
            txtbxFeature.Text = shellInfo.Feature;
            deleteMail.Checked = shellInfo.DeleteWhenFinished;
            nudWaitTime.Value = shellInfo.WaitTime / 1000;
            txtbxAdmin.Text = shellInfo.Administrator;
            txtbxFeature.Select();
        }

        /// <summary>
        /// 命令设置区域失效
        /// </summary>
        private void SettingUnEnable()
        {
            btnBegin.Enabled = false;
            btnCancel.Enabled = false;
            btnStop.Enabled = true;
            txtbxFeature.ReadOnly = true;
            txtbxAdmin.ReadOnly = true;
            nudWaitTime.ReadOnly = true;
            nudWaitTime.Enabled = false;
            deleteMail.Enabled = false;
            btnStop.Select();
        }

        /// <summary>
        /// 加载窗体
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainWindow_Load(object sender, EventArgs e)
        {
            txtbxUser.Text = shellInfo.User;
            txtbxPwd.Text = shellInfo.Password;
            txtbxHost.Text = shellInfo.Host;
            nudPort.Value = shellInfo.Port;
            txtbxFeature.Text = shellInfo.Feature;
            deleteMail.Checked = shellInfo.DeleteWhenFinished;
            nudWaitTime.Value = shellInfo.WaitTime / 1000;
            txtbxAdmin.Text = shellInfo.Administrator;

            SettingUnEnable();
            MailBoxSettingEnable();
            rtbxInstructionInfo.ForeColor = Color.Blue;
        }

        /// <summary>
        /// 应用邮箱设置
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnApply_Click(object sender, EventArgs e)
        {
            string user = txtbxUser.Text.Trim();
            string pwd = txtbxPwd.Text.Trim();
            string host = txtbxHost.Text.Trim();
            if (user == "")
            {
                MessageBox.Show("请输入用户名!");
            }
            else if (pwd == "")
            {
                MessageBox.Show("请输入密码!");
            }
            else if (host == "")
            {
                MessageBox.Show("请输入服务器地址!");
            }
            else
            {
                shellInfo.User = user;
                shellInfo.Password = pwd;
                shellInfo.Host = host;
                shellInfo.Port = (int)nudPort.Value;
                if (mailBoxShell.Connect())
                {
                    MailBoxSettingUnEnable();
                    SettingEnable();
                }
                else
                {
                    MessageBox.Show("登录失败,请检查设置!");
                }
                mailBoxShell.Close();
            }
        }

        /// <summary>
        /// 取消邮箱设置
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            SettingUnEnable();
            MailBoxSettingEnable();
        }

        /// <summary>
        /// 开始邮箱监听
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnBegin_Click(object sender, EventArgs e)
        {
            string feature = txtbxFeature.Text.Trim();
            if (feature == "")
            {
                MessageBox.Show("请输入口令!");
            }
            else
            {
                shellInfo.Administrator = txtbxAdmin.Text.Trim();
                shellInfo.Feature = feature;
                shellInfo.Frequency = (long)nudWaitTime.Value * 1000 / shellInfo.Delay;
                shellInfo.DeleteWhenFinished = deleteMail.Checked;
                shellInfo.LastChekedTime = DateTime.Now;
                mailBoxShell.AsynWaitingForInstruction();
                SettingUnEnable();
                rtbxInstructionInfo.Clear();
                rtbxInstructionInfo.AppendText("开始监听邮箱..." + System.Environment.NewLine + System.Environment.NewLine);
                statusLabel.Text = "工作中";
            }
        }

        /// <summary>
        /// 停止邮箱监听
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnStop_Click(object sender, EventArgs e)
        {
            shellInfo.isWorking = false;
            SettingEnable(); 
            rtbxInstructionInfo.AppendText("已停止监听" + System.Environment.NewLine);
        }

        /// <summary>
        /// 命令回显
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="mail"></param>
        public void InstructionArrived(object sender, LumiSoft.Net.Mail.Mail_Message mail)
        {
            rtbxInstructionInfo.AppendText("发件人:" + mail.From + System.Environment.NewLine);
            rtbxInstructionInfo.AppendText("时间:" + mail.Date + System.Environment.NewLine);
            rtbxInstructionInfo.AppendText("标题:" + mail.Subject + System.Environment.NewLine);
            rtbxInstructionInfo.AppendText(mail.BodyText + System.Environment.NewLine);
            rtbxInstructionInfo.AppendText("-------------------" + System.Environment.NewLine);
            rtbxInstructionInfo.SelectionStart = rtbxInstructionInfo.TextLength;
        }

        private void txtbxUser_TextChanged(object sender, EventArgs e)
        {
            txtbxPwd.Text = "";
            txtbxHost.Text = "";
        }

        private void MainWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (shellInfo.isWorking)
            {
                btnStop_Click(this, null);
            }
        }

        private void txtbxHost_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string[] a = txtbxHost.Text.Split('.');
                string t = "";
                int i = txtbxUser.Text.IndexOf("@") + 1;
                if (i > 0 && a.Length > 0)
                {
                    t = txtbxUser.Text.Substring(i);
                    string[] b = t.Split('.');
                    if (a.Length > 0 && b.Length > 0)
                    {
                        txtbxHost.Text = a[0];
                        for (i = 1; i < a.Length; i++)
                        {
                            if (a[i].Length > 0)
                            {
                                txtbxHost.Text += "." + a[i];
                            }
                            else
                            {
                                break;
                            }
                        }
                        for (i--; i < b.Length; i++)
                        {
                            txtbxHost.Text += "." + b[i];
                        }
                    }
                }
            }
        }

        public void mailBoxShell_NewMailArrived(object sender, int count)
        {
            statusLabel.Text = string.Format("收取时间:{0}(新邮件:{1})", DateTime.Now.ToShortTimeString(), count);
        }
    }
}
