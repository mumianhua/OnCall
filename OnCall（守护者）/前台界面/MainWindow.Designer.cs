﻿namespace 前台界面
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MailBoxSetting = new System.Windows.Forms.GroupBox();
            this.nudPort = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnApply = new System.Windows.Forms.Button();
            this.txtbxHost = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtbxPwd = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtbxUser = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.CommandsInfo = new System.Windows.Forms.GroupBox();
            this.rtbxInstructionInfo = new System.Windows.Forms.RichTextBox();
            this.Setting = new System.Windows.Forms.GroupBox();
            this.nudWaitTime = new System.Windows.Forms.NumericUpDown();
            this.deleteMail = new System.Windows.Forms.CheckBox();
            this.txtbxFeature = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnBegin = new System.Windows.Forms.Button();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.txtbxAdmin = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.menu = new System.Windows.Forms.MenuStrip();
            this.编辑ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.关于ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MailBoxSetting.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPort)).BeginInit();
            this.CommandsInfo.SuspendLayout();
            this.Setting.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudWaitTime)).BeginInit();
            this.statusStrip.SuspendLayout();
            this.menu.SuspendLayout();
            this.SuspendLayout();
            // 
            // MailBoxSetting
            // 
            this.MailBoxSetting.Controls.Add(this.nudPort);
            this.MailBoxSetting.Controls.Add(this.label6);
            this.MailBoxSetting.Controls.Add(this.btnCancel);
            this.MailBoxSetting.Controls.Add(this.btnApply);
            this.MailBoxSetting.Controls.Add(this.txtbxHost);
            this.MailBoxSetting.Controls.Add(this.label3);
            this.MailBoxSetting.Controls.Add(this.txtbxPwd);
            this.MailBoxSetting.Controls.Add(this.label2);
            this.MailBoxSetting.Controls.Add(this.txtbxUser);
            this.MailBoxSetting.Controls.Add(this.label1);
            this.MailBoxSetting.Location = new System.Drawing.Point(12, 28);
            this.MailBoxSetting.Name = "MailBoxSetting";
            this.MailBoxSetting.Size = new System.Drawing.Size(191, 169);
            this.MailBoxSetting.TabIndex = 3;
            this.MailBoxSetting.TabStop = false;
            this.MailBoxSetting.Text = "邮箱设置";
            // 
            // nudPort
            // 
            this.nudPort.Location = new System.Drawing.Point(71, 107);
            this.nudPort.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nudPort.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudPort.Name = "nudPort";
            this.nudPort.Size = new System.Drawing.Size(100, 21);
            this.nudPort.TabIndex = 4;
            this.nudPort.Value = new decimal(new int[] {
            110,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(30, 112);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 12);
            this.label6.TabIndex = 9;
            this.label6.Text = "端口:";
            // 
            // btnCancel
            // 
            this.btnCancel.Enabled = false;
            this.btnCancel.Location = new System.Drawing.Point(97, 136);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(79, 23);
            this.btnCancel.TabIndex = 12;
            this.btnCancel.Text = "取消";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnApply
            // 
            this.btnApply.Location = new System.Drawing.Point(12, 136);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(79, 23);
            this.btnApply.TabIndex = 5;
            this.btnApply.Text = "应用";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // txtbxHost
            // 
            this.txtbxHost.Location = new System.Drawing.Point(71, 80);
            this.txtbxHost.Name = "txtbxHost";
            this.txtbxHost.Size = new System.Drawing.Size(100, 21);
            this.txtbxHost.TabIndex = 3;
            this.txtbxHost.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtbxHost_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 12);
            this.label3.TabIndex = 3;
            this.label3.Text = "服务器:";
            // 
            // txtbxPwd
            // 
            this.txtbxPwd.Location = new System.Drawing.Point(71, 53);
            this.txtbxPwd.Name = "txtbxPwd";
            this.txtbxPwd.PasswordChar = '*';
            this.txtbxPwd.Size = new System.Drawing.Size(100, 21);
            this.txtbxPwd.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(30, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "密码:";
            // 
            // txtbxUser
            // 
            this.txtbxUser.Location = new System.Drawing.Point(71, 26);
            this.txtbxUser.Name = "txtbxUser";
            this.txtbxUser.Size = new System.Drawing.Size(100, 21);
            this.txtbxUser.TabIndex = 1;
            this.txtbxUser.TextChanged += new System.EventHandler(this.txtbxUser_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "邮箱:";
            // 
            // CommandsInfo
            // 
            this.CommandsInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.CommandsInfo.Controls.Add(this.rtbxInstructionInfo);
            this.CommandsInfo.Location = new System.Drawing.Point(12, 203);
            this.CommandsInfo.Name = "CommandsInfo";
            this.CommandsInfo.Size = new System.Drawing.Size(363, 215);
            this.CommandsInfo.TabIndex = 4;
            this.CommandsInfo.TabStop = false;
            this.CommandsInfo.Text = "命令详情";
            // 
            // rtbxInstructionInfo
            // 
            this.rtbxInstructionInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbxInstructionInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbxInstructionInfo.Location = new System.Drawing.Point(3, 17);
            this.rtbxInstructionInfo.Name = "rtbxInstructionInfo";
            this.rtbxInstructionInfo.ReadOnly = true;
            this.rtbxInstructionInfo.Size = new System.Drawing.Size(357, 195);
            this.rtbxInstructionInfo.TabIndex = 0;
            this.rtbxInstructionInfo.TabStop = false;
            this.rtbxInstructionInfo.Text = "";
            // 
            // Setting
            // 
            this.Setting.Controls.Add(this.txtbxAdmin);
            this.Setting.Controls.Add(this.label7);
            this.Setting.Controls.Add(this.nudWaitTime);
            this.Setting.Controls.Add(this.deleteMail);
            this.Setting.Controls.Add(this.txtbxFeature);
            this.Setting.Controls.Add(this.label5);
            this.Setting.Controls.Add(this.label4);
            this.Setting.Controls.Add(this.btnStop);
            this.Setting.Controls.Add(this.btnBegin);
            this.Setting.Location = new System.Drawing.Point(210, 29);
            this.Setting.Name = "Setting";
            this.Setting.Size = new System.Drawing.Size(167, 168);
            this.Setting.TabIndex = 6;
            this.Setting.TabStop = false;
            this.Setting.Text = "命令";
            // 
            // nudWaitTime
            // 
            this.nudWaitTime.Location = new System.Drawing.Point(61, 52);
            this.nudWaitTime.Maximum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.nudWaitTime.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudWaitTime.Name = "nudWaitTime";
            this.nudWaitTime.Size = new System.Drawing.Size(100, 21);
            this.nudWaitTime.TabIndex = 7;
            this.nudWaitTime.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // deleteMail
            // 
            this.deleteMail.AutoSize = true;
            this.deleteMail.Location = new System.Drawing.Point(22, 106);
            this.deleteMail.Name = "deleteMail";
            this.deleteMail.Size = new System.Drawing.Size(72, 16);
            this.deleteMail.TabIndex = 9;
            this.deleteMail.Text = "删除目标";
            this.deleteMail.UseVisualStyleBackColor = true;
            // 
            // txtbxFeature
            // 
            this.txtbxFeature.Location = new System.Drawing.Point(61, 25);
            this.txtbxFeature.Name = "txtbxFeature";
            this.txtbxFeature.Size = new System.Drawing.Size(100, 21);
            this.txtbxFeature.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 12);
            this.label5.TabIndex = 6;
            this.label5.Text = "延时:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 12);
            this.label4.TabIndex = 6;
            this.label4.Text = "口令:";
            // 
            // btnStop
            // 
            this.btnStop.Enabled = false;
            this.btnStop.Location = new System.Drawing.Point(93, 135);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(68, 23);
            this.btnStop.TabIndex = 11;
            this.btnStop.Text = "停止";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnBegin
            // 
            this.btnBegin.Enabled = false;
            this.btnBegin.Location = new System.Drawing.Point(6, 135);
            this.btnBegin.Name = "btnBegin";
            this.btnBegin.Size = new System.Drawing.Size(68, 23);
            this.btnBegin.TabIndex = 10;
            this.btnBegin.Text = "开始";
            this.btnBegin.UseVisualStyleBackColor = true;
            this.btnBegin.Click += new System.EventHandler(this.btnBegin_Click);
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 421);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(389, 22);
            this.statusStrip.SizingGrip = false;
            this.statusStrip.TabIndex = 7;
            this.statusStrip.TabStop = true;
            this.statusStrip.Text = "statusStrip1";
            // 
            // statusLabel
            // 
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // txtbxAdmin
            // 
            this.txtbxAdmin.Location = new System.Drawing.Point(61, 79);
            this.txtbxAdmin.Name = "txtbxAdmin";
            this.txtbxAdmin.Size = new System.Drawing.Size(100, 21);
            this.txtbxAdmin.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 82);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 11;
            this.label7.Text = "管理员 :";
            // 
            // menu
            // 
            this.menu.BackColor = System.Drawing.SystemColors.ControlLight;
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.编辑ToolStripMenuItem,
            this.关于ToolStripMenuItem});
            this.menu.Location = new System.Drawing.Point(0, 0);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(389, 24);
            this.menu.TabIndex = 8;
            this.menu.Text = "menuStrip1";
            // 
            // 编辑ToolStripMenuItem
            // 
            this.编辑ToolStripMenuItem.Name = "编辑ToolStripMenuItem";
            this.编辑ToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.编辑ToolStripMenuItem.Text = "编辑";
            // 
            // 关于ToolStripMenuItem
            // 
            this.关于ToolStripMenuItem.Name = "关于ToolStripMenuItem";
            this.关于ToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.关于ToolStripMenuItem.Text = "关于";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(389, 443);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.menu);
            this.Controls.Add(this.Setting);
            this.Controls.Add(this.CommandsInfo);
            this.Controls.Add(this.MailBoxSetting);
            this.MainMenuStrip = this.menu;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(405, 1024);
            this.MinimumSize = new System.Drawing.Size(405, 350);
            this.Name = "MainWindow";
            this.Text = "On Call @LYC";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainWindow_FormClosing);
            this.Load += new System.EventHandler(this.MainWindow_Load);
            this.MailBoxSetting.ResumeLayout(false);
            this.MailBoxSetting.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPort)).EndInit();
            this.CommandsInfo.ResumeLayout(false);
            this.Setting.ResumeLayout(false);
            this.Setting.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudWaitTime)).EndInit();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.menu.ResumeLayout(false);
            this.menu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox MailBoxSetting;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.TextBox txtbxHost;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtbxPwd;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtbxUser;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox CommandsInfo;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.RichTextBox rtbxInstructionInfo;
        private System.Windows.Forms.GroupBox Setting;
        private System.Windows.Forms.NumericUpDown nudWaitTime;
        private System.Windows.Forms.CheckBox deleteMail;
        private System.Windows.Forms.TextBox txtbxFeature;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnBegin;
        private System.Windows.Forms.NumericUpDown nudPort;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel statusLabel;
        private System.Windows.Forms.TextBox txtbxAdmin;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.MenuStrip menu;
        private System.Windows.Forms.ToolStripMenuItem 编辑ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 关于ToolStripMenuItem;
    }
}